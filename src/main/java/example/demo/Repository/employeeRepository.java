package example.demo.Repository;

import example.demo.Entity.employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface employeeRepository extends JpaRepository<employee, String> {
    @Query(value = "select * from employee d where d.id = :p_id ", nativeQuery = true)
    employee findEmById(@Param("p_id") String p_id);
}
