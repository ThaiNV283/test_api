package example.demo.Services;

import example.demo.Entity.employee;
import example.demo.model.employeeDto;

import java.util.List;

public interface employeeServices {
    List<employee> findAll();

    employee findEmById(String id);

    employee createEm(employeeDto employeeDto);

    boolean updateEm(employee employee);

    boolean deleteEm(String id);
}
