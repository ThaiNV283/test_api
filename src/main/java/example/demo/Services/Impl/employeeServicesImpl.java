package example.demo.Services.Impl;

import example.demo.Entity.employee;
import example.demo.Repository.employeeRepository;
import example.demo.Services.employeeServices;
import example.demo.model.employeeDto;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class employeeServicesImpl implements employeeServices {
    Logger logger = LoggerFactory.getLogger(employeeServicesImpl.class);

    @Autowired
    private employeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<employee> findAll() {
        List<employee> employeeList = new ArrayList<>();
        try {
            employeeList = employeeRepository.findAll();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return employeeList;
    }

    @Override
    public employee findEmById(String id) {
        employee employee = new employee();
        employee = employeeRepository.findEmById(id);
        return employee;
    }

    @Override
    public employee createEm(employeeDto employeeDto) {
        logger.info("date_oki1" + employeeDto.getBirthday());
        try {
            if (employeeDto != null && !employeeRepository.existsById(employeeDto.getId())) {
                employeeRepository.save(modelMapper.map(employeeDto, employee.class));
                return modelMapper.map(employeeDto, employee.class);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public boolean updateEm(employee employee) {
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            if (employee != null && employeeRepository.existsById(employee.getId())) {
                employeeRepository.save(employee);
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }

    @Override
    public boolean deleteEm(String id) {
        try {
            employee employee1 = new employee();
            employee1 = employeeRepository.findEmById(id);
            if (employee1 != null) {
                employeeRepository.deleteById(employee1.getId());
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return false;
    }
}
