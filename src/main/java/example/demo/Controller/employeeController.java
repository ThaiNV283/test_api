package example.demo.Controller;

import example.demo.Entity.employee;
import example.demo.Services.employeeServices;
import example.demo.model.employeeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("")
public class employeeController {
    @Autowired
    private employeeServices employeeServices;

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public ResponseEntity<List<employee>> getAllEm() {
        List<employee> listEm = new ArrayList<>();
        listEm = employeeServices.findAll();
        return new ResponseEntity<>(listEm, HttpStatus.OK);
    }

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public ResponseEntity<employee> emInfo(@RequestBody employee employee) {
        employee em = new employee();
        em = employeeServices.findEmById(employee.getId());
        return new ResponseEntity<>(em, HttpStatus.OK);
    }

    @RequestMapping(value = "/createEm", method = RequestMethod.POST)
    public ResponseEntity<employee> createEm(@RequestBody employeeDto employeeDto) {
        employee em = new employee();
        em = employeeServices.createEm(employeeDto);
        return new ResponseEntity<>(em, HttpStatus.OK);
    }

    @RequestMapping(value = "/updateEm", method = RequestMethod.PUT)
    public boolean updateEm(@RequestBody employee employee) {
        return employeeServices.updateEm(employee);
    }

    @RequestMapping(value = "deleteEm", method = RequestMethod.GET)
    public boolean deleteEm(@RequestParam("id") String id) {
        return employeeServices.deleteEm(id);
    }
}
