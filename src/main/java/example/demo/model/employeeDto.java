package example.demo.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class employeeDto implements Serializable {
    private String id;
    private String name;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date birthday;
    private String phone;
    private String email;
}
