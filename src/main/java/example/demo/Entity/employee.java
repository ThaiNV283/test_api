package example.demo.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "employee")
@Getter
@Setter
public class employee {
    @Id
    private String id;
    private String name;
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date birthday;
    private String phone;
    private String email;
}
